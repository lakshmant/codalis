<?php
/**
 * Created by PhpStorm.
 * User: Bobaminions
 * Date: 8/15/2018
 * Time: 2:41 PM
 */

$content = "<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
    <h3>Aliquam mauris diam, gravida eget finibus varius.</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br/>Aliquam mauris diam, gravida eget finibus varius.</p><img src='contents/home/banner-img01_1920x640.jpg' /><a href='' class='btn btn-gradient-primary'>En saviour plus</a> ";

//$tags_array = ['h3', 'h4', 'img'];

/**
 * @param $content
 * @return string
 * @todo One can improve and refactor the code!
 */

function editor_content_dom_manipulation ($content) {

    $dom = new DOMDocument();
    $dom->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

    // Evaluate required tags in HTML. This just shows
    // that you can be more selective on your tags

    $xpath = new DOMXPath($dom);
    $tag_subtitle = $xpath->evaluate("//h4");
    $tag_title_h3 = $xpath->evaluate("//h3");
    $tag_img = $xpath->evaluate("//img");

    $tag_subtitle[0]->setAttribute("class", 'entry-card__subtitle');
    $tag_title_h3[0]->setAttribute("class", 'entry-card__title');
    $tag_img[0]->setAttribute("class", 'entry-card__img');

    $figure_element = $dom->createElement('figure');
    $figure_element->setAttribute("class", 'entry-card__picture');

    $tag_img[0]->parentNode->insertBefore($figure_element, $tag_img[0]);
    $figure_element->appendChild($tag_img[0]);

    // Save the HTML changes
    $content = $dom->saveHTML();

    return $content;
}

echo editor_content_dom_manipulation($content);