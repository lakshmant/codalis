/**
 * @name: ps-custom-navbar.js
 * @desc: contains sticky header script
 */

/*!
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function (w, d, $, undefined) {
    /**
     * Don't write any code above this except comment because "use strict" directive won't have any effetc
     * Enable strict mode to prevent script to run in safe mode (modern way - ECMA2016)
     */
    // "use strict";

    var $doc = $('body');
    var $mastHeader = $('#mastHead');
    var $jsRequestQuoteNode = $('.js-request-quote-target');
    var lastScrollTop = 0;

    /**
     * Detect scroll direction and change the behavior of the header on page scroll
     */

    $(w).scroll(function (event) {
        var $self = $(this);
        var scrollTop = $self.scrollTop();

        /* =========== Home page sticky header feature ============ */

        // Toggle class for sticky header
        if ($doc.hasClass('home')) {
            if (scrollTop >= 20) {
                if (window.docClickFlag === false) $mastHeader.addClass('is-stuck');
            } else {
                $mastHeader.removeClass('is-stuck');
            }
        }

        /* =========== Inner page sticky header feature ============ */

        if (scrollTop > window.headerHeight) {
            // Reset navbar :: Reference to script.js
            window.resetNavbarOnScroll();

            if (scrollTop > lastScrollTop) {
                if (docClickFlag === false) $doc.addClass('page-scrolled-down').removeClass('page-scrolled-up');
            } else if (scrollTop === lastScrollTop) {
                //do nothing
                //In IE this is an important condition because there seems to be some instances where the last scrollTop is equal to the new one
            } else {
                if (docClickFlag === false) $doc.addClass('page-scrolled-up').removeClass('page-scrolled-down');
            }
        }

        if (scrollTop <= window.headerHeight) {
            if (docClickFlag === false) $doc.removeClass('page-scrolled-up page-scrolled-down');
        }

        lastScrollTop = scrollTop;

        function _ps_alignRequestQuoteBtn() {
            $('.sticky-request-quote-holder').css({
                width: parseInt(($(w).innerWidth() - $('.topnavbar__c>.container').outerWidth()) / 2 + 180)
            });
        }

        if ($doc.hasClass('home')) {
            // Request a quote sticky feature
            if (scrollTop >= $jsRequestQuoteNode.offset().top + $jsRequestQuoteNode.outerHeight() - $mastHeader.outerHeight()) {
                $doc.addClass('is-request-quote-shown');
            } else {
                $doc.removeClass('is-request-quote-shown');
            }
            _ps_alignRequestQuoteBtn();
        }
    });

})(window, document, jQuery, undefined);