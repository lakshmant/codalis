var ps_constructSubMenuOnPageLoad = true;
ps_constructSubMenu($('.js-submenu-link'), $('.js-submenu-category'), $('.js-submenu-subcategory'), $('.js-submenu-desc-holder'), $('.submenu-holder'));

function ps_constructSubMenu (_self, _nodeCategory, _nodeSubCategory, _nodeCategoryDesc, _parent) {
    var _iterator;
    if (!ps_constructSubMenuOnPageLoad) {
        _iterator = _self;
    } else {
        _iterator = _nodeCategoryDesc;
    }

    _iterator.each(function() {
        var $self = $(this);
        var _categoryHeight = parseInt($self.closest(_nodeCategory).outerHeight());
        var _subCategoryHeight = parseInt($self.closest(_nodeCategory).find(_nodeSubCategory).outerHeight());
        var _parentPadTop = parseInt($self.closest(_parent).css('padding-top'));
        var _parentPadBtm = parseInt($self.closest(_parent).css('padding-bottom'));
        var finalHeight;

        var _categoryDescHeight;
        if ($self === _nodeCategoryDesc) {
            _categoryDescHeight = $self.outerHeight();
        } else {
            _categoryDescHeight = parseInt($self.closest(_nodeCategory).find(_nodeSubCategory).find('.is-active').find(_nodeCategoryDesc).outerHeight());
        }

        // == Create array of different height values
        var _heightArray = [];

        // == Push multiple values into empty array
        _heightArray.push.apply(_heightArray, [_categoryHeight, _subCategoryHeight, _categoryDescHeight]);

        // == Get max value from target array
        finalHeight = Math.max.apply(Math, _heightArray);

        // == Update final value with max value and the padding-y values of parent node
        finalHeight += _parentPadTop + _parentPadBtm;

        // == Set final value of the height to the parent node
        $self.closest(_parent).css({
            'height': finalHeight
        });
    });
}