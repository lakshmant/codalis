/**
 * Custom map script
 */

/**
 * @desc: Theme's custom map javascript code
 * Prepending ';' infront of the javascript closure to prevent unnecessary errors that might arise from the files included prior to this.
 * Also wrapping entire code into a javascript closure show that the code won't be visible outside the closure itself!!!
 */

;(function () {
    //Load Google map
    loadMap($('.js-map-node'));

    //Function to load map on event details page
    function loadMap($map) {
        if ($map.length === 0) return;
        $map.each(function (index, elem) {
            var $self = $(this),
                latLngDataVal = $self.attr('data-map-latlan'),
                mapLatLanArray = latLngDataVal.split(","),
                dataZoom = $self.attr('data-zoom') * 1,
                isGrayScale = 'undefined' !== typeof $self.attr('data-is-grayscale') ? $self.attr('data-is-grayscale') : '',
                dataLink = $self.attr('data-link'),
                dataMakers = $self.attr('data-map-markers');
                dataMakerArray = dataMakers.split(", ");

            var mapLatLan = new google.maps.LatLng(mapLatLanArray[0], mapLatLanArray[1]);
            var map;

            function initialize() {
                var mapOptions = {
                    scrollwheel: false,
                    center: mapLatLan,
                    zoom: dataZoom,
                    fullscreenControlOptions: {
                      position: google.maps.ControlPosition.TOP_LEFT
                    },
                    disableDefaultUI: false,
                    mapTypeControl: false,
                    streetViewControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    styles: isGrayScale === 'true' ? [{
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{"color": "#dadada"}, {"lightness": 5}]
                    }, {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
                    }, {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
                    }, {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{"color": "#dedede"}, {"lightness": 21}]
                    }, {
                        "elementType": "labels.text.stroke",
                        "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
                    }, {
                        "elementType": "labels.text.fill",
                        "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
                    }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
                    }, {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
                    }, {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
                    }] : '',

                };
                map = new google.maps.Map(elem, mapOptions);

                // == Map marker
                var marker = new google.maps.Marker({
                    icon: dataMakerArray[0],
                    animation: google.maps.Animation.BOUNCE,
                    position: mapLatLan,
                    map: map,
                });

                // == Switch map pin icon on small and large device
                function switchMarkerPin () {
                    if (window.innerWidth <= 1023) {
                        marker.setIcon(dataMakerArray[1]);
                    } else {
                        marker.setIcon(dataMakerArray[0]);
                    }
                }

                switchMarkerPin();

                //map resize event
                var resizeTimer = null;
                google.maps.event.addDomListener(window, 'resize', function () {
                    if (resizeTimer) clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function () {
                        map.setCenter(mapOptions.center);
                        switchMarkerPin();
                    }, 10);
                });

                // Attach click event on marker
                google.maps.event.addListener(marker, 'click', function () {
                    window.open(dataLink, '_blank');
                });
            }

            // Load map on google library load completes
            google.maps.event.addDomListener(window, 'load', initialize);
        });
    }
})(jQuery);