/**
 * Created by Procab on 1/10/2018.
 */

window.onload = function() {
    var inputsWithLimit = document.querySelectorAll('textarea[maxlength]');
    console.log('length is'+inputsWithLimit.length);
    if(inputsWithLimit.length > 0){
        inputsWithLimit.forEach(function(inputWithLimit){
            var hint = document.createElement('div');
            //hint.classList.add('txt-limit-hint');
            hint.style.color = "#D93600";
            //https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore
            inputWithLimit.parentNode.insertBefore(hint, inputWithLimit.nextSibling);
            //event
            inputWithLimit.addEventListener('input', function(event){
                var inputMaxLength = event.target.getAttribute('maxlength');
                //append the message in the div tag
                hint.innerHTML = 'Total Characters : '+inputMaxLength +'<br/> Remaining : ' + (inputMaxLength - event.target.value.length);
            });
        });
    }
}
