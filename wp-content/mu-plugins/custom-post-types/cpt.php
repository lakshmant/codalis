<?php
namespace App\Cpt;


/**
 * @param $name
 * @param array $args
 * https://developer.wordpress.org/resource/dashicons/#schedule
 */
function add_post_type($name, $args = array()){
    add_action('init', function() use($name, $args){
        $upper = ucwords($name);
        $name = strtolower(str_replace(' ', '_', $name));
        $args = array_merge(
            [
                'menu_icon' => 'dashicons-admin-home',
                'public'	=> true,
                'label'		=> "$upper",
                'labels'	=> ['add_new_item'=>"Add new $upper"],
                'supports' 	=> ['title','editor','thumbnail','page-attributes'],
                'exclude_from_search' => false,
                'capability_type'    => 'page'
            ],
            $args);
        register_post_type($name, $args);
    });
}
function add_taxonomy($name, $post_type, $args = array()){
    $name = strtolower($name);
    add_action('init', function() use($name, $post_type, $args){
        $args = array_merge(
            [
                'label' => ucwords($name),
                'show_ui' => true,
                'query_var' => true
            ],
            $args);
        //echo '<pre>';	var_dump($args);	echo '</pre>';
        register_taxonomy($name, $post_type,  $args);
    });
}

add_post_type ( 'member', array (
    'name' => 'member',
    'menu_icon'   => 'dashicons-groups',
    'supports' 	=> ['title','thumbnail','editor'],
    'label' => "Member",
    'show_admin_column' => true
    // 'hierarchical' => true,
    //'taxonomies' => ['logo','promo_category'],
    //'has_archive' => 'presse'
));

add_post_type ( 'case-study', array (
    'name' => 'Case Study',
    'menu_icon' => 'dashicons-admin-page',
    'supports' 	=> ['title','thumbnail','editor'],
    'rewrite' => [
        'slug' => 'case-studies'
    ],

    'label' => "Case Study",
    // 'hierarchical' => true,
    'taxonomies' => ['post_tag'],
    //'has_archive' => 'presse'
));

add_post_type ( 'nos-technologies', array (
    'name' => 'Nos technologies',
    'menu_icon' => 'dashicons-admin-page',
    'supports' 	=> ['title','thumbnail','editor'],
    'label' => 'Nos technologies',
    // 'hierarchical' => true,
    'rewrite' => [
        'slug' => 'nos-technologies'
    ],
    //'taxonomies' => ['category'],
    //'has_archive' => 'presse'
));

add_taxonomy('member-category','member', [
    'label' => "Categories",
    'show_admin_column' => true,
    'hierarchical' => true,
]);


add_taxonomy('technology-category','nos-technologies', [
    'label' => "Categories",
    'show_admin_column' => true,
    'hierarchical' => true,
]);

add_taxonomy('casestudy-category','case-study', [
    'label' => "Categories",
    'show_admin_column' => true,
    'hierarchical' => true,
]);