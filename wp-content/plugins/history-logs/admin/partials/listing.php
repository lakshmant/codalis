<?php
/**
* Template file to render listing table for admin.
*
* @link       https://github.com/faiyazalam
*
* @package    User_Login_History
* @subpackage User_Login_History/admin/partials
*/
?>
<div class="wrap">
  <h1>History logs</h1>
  <p>Times are displayed in UTC format</p>
  <div class="listingOuter">
    <form method="post">
      <input type="hidden" name="<?php echo $this->plugin_name.(is_network_admin()?'_network_admin_listing_table':'_admin_listing_table')?>" value="">
      <?php
      $this->list_table->display();
      ?>
    </form>
  </div>
</div>
