<li class="menu__item has-submenu menu-lg menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu--lg ">
    <a href="#" class="menu__link js-menu-link">Vos besoins <i class="icon icon-right-chev mr-0"></i></a>
    <div class="submenu-holder">
        <div class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>Vos besoins</div>
        <div class="row submenu-grid">
            <div class="col-12 col-md-4 submenu-col">
                <ul class="submenu-category">
                    <li class="submenu-category__item"><a href="#" class="submenu-category__link js-submenu-link"  data-target="submenu-154">INFRASTRUCTURE IT</a>
                        <div class="submenu-content-container submenu-154 js-submenu-content-container is-shown">
                            <div class="row submenu-category-grid">
                                <div class="col-12 col-sm-6 submenu-category-col-sm">
                                    <ul class="submenu-subcategory">
                                        <li class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>INFRASTRUCTURE IT</li>
                                        <li class="submenu-category__item"><a href="#" class="submenu-category__link ">Infrastructure IT et cloud</a></li>
                                        <li class="submenu-category__item"><a href="#" class="submenu-category__link ">Systèmes et réseaux</a></li>
                                        <li class="submenu-category__item"><a href="#" class="submenu-category__link ">Sécurité</a></li>
                                        <li class="submenu-category__item"><a href="#" class="submenu-category__link ">Monitoring et sous-traitance</a></li>
                                    </ul>
                                    1</div>
                            </div>
                        </div>
                    </li>

                    <li class="submenu-category__item"><a href="#" class="submenu-category__link js-submenu-link" >Workplace digital et mobile</a></li>
                    <li class="submenu-category__item"><a href="#" class="submenu-category__link js-submenu-link" >Consulting</a></li>
                    <li class="submenu-category__item"><a href="#" class="submenu-category__link js-submenu-link" >Innovation</a></li></ul>
                0</div></div></div>
</li>





<!-- compare with -->
<li class="menu__item has-submenu has-submenu--lg">
    <a href="" class="menu__link js-menu-link">Vos besoins <i class="icon icon-right-chev mr-0"></i></a>
    <div class="submenu-holder">
        <div class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>Vos besoins</div>
        <div class="row submenu-grid">
            <div class="col-12 col-md-4 submenu-col">
                <ul class="submenu-category js-submenu-category">

                    <li class="submenu-category__item is-active">
                        <a href="" class="submenu-category__link text-uppercase js-submenu-link" data-target="submenu-subcategory-infrastructure-it">Infrastructure it</a>
                        <div class="submenu-content-container submenu-subcategory-infrastructure-it js-submenu-content-container is-shown">
                            <div class="row submenu-category-grid">
                                <div class="col-12 col-sm-6 submenu-category-col-sm">
                                    <ul class="submenu-subcategory">
                                        <li class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>Infrastructure IT</li>
                                        <li class="submenu-category__item is-active">
                                            <a href="" class="submenu-category__link">Infrastructure IT et cloud</a></li>
                                        <li class="submenu-category__item">
                                            <a href="" class="submenu-category__link">Systèmes et réseaux</a>
                                        </li>
                                        <li class="submenu-category__item"><a href="" class="submenu-category__link">Sécurité </a></li>
                                        <li class="submenu-category__item"><a href="" class="submenu-category__link" data-target="">Monitoring et sous-traitance </a></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-sm-6 submenu-category-col-sm">
                                    <aside class="submenu-category__desc">
                                        <figure><img alt="Category Name" class="img" src="contents/megamenu/placeholder-img287x174.jpg" /></figure>
                                        <blockquote><p>« Lorem ipsum dolor sit amet it consectetur » « Lorem ipsum dolor sit amet it consectetur » « Lorem ipsum dolor sit amet it consectetur » « Lorem ipsum dolor sit amet it consectetur » « Lorem ipsum dolor sit amet it consectetur » « Lorem ipsum dolor sit amet it consectetur » « Lorem ipsum dolor sit amet it consectetur »</p></blockquote>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="submenu-category__item">
                        <a href="" class="submenu-category__link text-uppercase js-submenu-link" data-target="submenu-subcategory-workplace-digital">Workplace digital et mobile</a>
                        <div class="submenu-content-container submenu-subcategory-workplace-digital js-submenu-content-container">
                            <div class="row submenu-category-grid">
                                <div class="col-12 col-sm-6 submenu-category-col-sm">
                                    <ul class="submenu-subcategory">
                                        <li class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>Infrastructure IT</li>
                                        <li class="submenu-category__item">
                                            <a href="" class="submenu-category__link">Infrastructure IT et cloud</a></li>
                                        <li class="submenu-category__item">
                                            <a href="" class="submenu-category__link">Systèmes et réseaux</a>
                                        </li>
                                        <li class="submenu-category__item"><a href="" class="submenu-category__link">Sécurité </a></li>
                                        <li class="submenu-category__item"><a href="" class="submenu-category__link" data-target="">Monitoring et sous-traitance </a></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-sm-6 submenu-category-col-sm">
                                    <aside class="submenu-category__desc">
                                        <figure><img alt="Category Name" class="img" src="contents/megamenu/placeholder-img287x174.jpg" /></figure>
                                        <blockquote><p>« Lorem ipsum dolor sit amet it consectetur, Lorem ipsum dolor sit amet it consectetur... »</p></blockquote>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="submenu-category__item"><a href="" class="submenu-category__link text-uppercase js-submenu-link" data-target="consulting">Consulting</a></li>
                    <li class="submenu-category__item"><a href="" class="submenu-category__link text-uppercase js-submenu-link" data-target="innovation">Innovation</a></li>
                </ul>
            </div>
        </div>
    </div>
</li>

<!-- end comapre with -->
<li class="menu__item"><a href="#" class="menu__link">Nos technologies</a></li>
<li class="menu__item"><a href="#" class="menu__link">Ils nous font confiance</a></li>
<li class="menu__item has-submenu menu-normal menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children has-submenu--sm ">
    <a href="#" class="menu__link js-menu-link">Nous connaître <i class="icon icon-right-chev mr-0"></i></a>
    <div class="submenu-holder"><div class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>Nous connaître</div>
        <ul class="submenu-category"><li class="submenu-category__item"><a href="#" class="submenu-category__link text-uppercase" >Histoire</a></li>
            <li class="submenu-category__item"><a href="#" class="submenu-category__link text-uppercase" >Itconsectetur</a></li>
            <li class="submenu-category__item"><a href="#" class="submenu-category__link text-uppercase" >Dolor sit amet</a></li>
            <li class="submenu-category__item"><a href="#" class="submenu-category__link text-uppercase" >typesetting</a></li>
        </ul>
    </div>
</li>
<li class="menu__item"><a href="#" class="menu__link">Contact</a></li>
<li class="menu__item"><a href="#" class="menu__link">News</a></li>