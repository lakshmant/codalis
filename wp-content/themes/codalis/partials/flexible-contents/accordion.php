<?php
$type = $block['type'];
$contents = $block['contents'];
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
//$text = $block["text"];

if ($type=="Small" || $type=="Large"):
    if($type=="Small") {
        $accordionClass = "accordion--collapsable";
        $accordion_title_start_tag = "<h4 class='accordion-panel__title mb-0'>";
        $accordion_title_end_tag = "</h4>";
    }else {
        $accordionClass = "accordion--lg ";
        $accordion_title_start_tag = "<h3 class='accordion-panel__title mb-0'>";
        $accordion_title_end_tag = "</h3>";
    }
endif;
if(!empty($contents)):
    if($type=='Tab') {
        include 'accordion/tab.php';
    }else {
    ?>
    <section class="block block--accordion <?=$bgColor?> <?=$spaceType?> is-extended">
        <div class="block__body">
            <?php if(!empty($block['text'])): ?>
                <div class="mb-5"><?=$block['text']?></div>
            <?php endif; ?>
            <div class="accordion-container">
                <div class="accordion <?=$accordionClass?> js-accordion" data-scroll-active="true" data-multiple-option="true">
                    <?php
                    $count=1;
                    foreach ($contents as $content) :
                        if(!empty($content['active'])) :
                            $active = "active";
                            $isactive = "is-active";
                            $expandable = "expanded";
                            $style='style="display: block;"';
                        else :
                            $active = "";
                            $isactive = "";
                            $expandable = "";
                            $style='style="display: none;"';
                        endif;
                        $dataToggleTarget = "js-accordion-target-panel".$count;
                        $title = $content['title'];
                        $text = $content['text'];
                        $table = $content['table'];
                        ?>
                        <div class="accordion-panel <?=$active?>">
                            <?php if(!empty($title)): ?>
                                <div class="accordion-panel__header">
                                    <?= $accordion_title_start_tag; ?><a href="" class="accordion-panel__link js-accordion__toggle <?=$expandable?>" data-toggle-target="<?=$dataToggleTarget?>" target="_blank"><?=$title?></a><?= $accordion_title_end_tag; ?>
                                </div>
                            <?php endif; ?>
                            <div class="accordion-panel__collapse <?=$dataToggleTarget?> <?=$isactive?>" <?=$style?>>
                                <div class="accordion-panel__body">
                                    <?php
                                    if($content['content_type']=='text-table') :
                                            include 'accordion/text-table.php';
                                    endif;
                                    if($content['content_type']=='youtube-video') {
                                      include 'accordion/youtube-video.php';
                                    }
                                     if($content['content_type']=='two-cols') {
                                         include 'accordion/two-cols.php';
                                     } ?>
                                </div>
                            </div>
                        </div>
                        <?php $count++; endforeach; ?>
                </div><!-- /.Accordion module ends -->
            </div>
        </div>
    </section>
<?php } endif;