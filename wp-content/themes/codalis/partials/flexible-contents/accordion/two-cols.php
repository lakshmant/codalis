<?php
$image = $content['image'];
$image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_TWO_COLS);
$title2 = $content['title_2'];
$text2 = $content['text_2'];
$link2 = $content['link_2'];
$image2 = $content['image_2'];
$image2 = \App\getImageManager()->resize( \App\getImageDirectoryPath($image2), \App\IMAGE_SIZE_TWO_COLS);
?>
<section class="block is-extended">
    <div class="block__body">
        <div class="row row-gutter-30">
            <div class="col-sm-6 mb-4 mb-sm-0">
                <article class="entry-card entry-card--sm entry-card--flat">
                    <div class="entry-card__body">
                        <?php echo App\app_add_class($text); ?>
                    </div>
                    <?php if(!empty($image)) : ?>
                        <figure class="entry-card__picture zoom-effect-holder mb-0">
                            <img alt="Article thumbnail0" class="entry-card__img img img-full" src="<?php echo $image; ?>">
                        </figure>
                    <?php endif; ?>
                </article>
            </div>
            <div class="col-sm-6">
                <article class="entry-card entry-card--sm entry-card--flat">
                    <div class="entry-card__body">
                        <?php echo App\app_add_class($text2); ?>
                    </div>
                    <figure class="entry-card__picture zoom-effect-holder mb-0">
                        <img alt="Article thumbnail0" class="entry-card__img img img-full" src="<?php echo $image2; ?>">
                    </figure>
                </article>
            </div>
        </div>
    </div>
</section>