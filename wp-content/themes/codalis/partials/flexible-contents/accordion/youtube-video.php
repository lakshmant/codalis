<?php
$videoUrl = $content['youtube_video_url'];
$youTubeVideoID = app\youtube_video_id($videoUrl);
?>
<section class="block block--video text-center is-extended">
    <div class="block__body">
        <div class="video-container">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?=$youTubeVideoID?>?autoplay=0;rel=0;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
</section><!-- /.Internal video section ends -->