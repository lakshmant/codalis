<?php
    $title = $block['title'];
    $link = $block['link'];
    $spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
    $bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
    $target = !empty($link['target'])?'target="_blank"':'';
?>
<section class="block block--intro position-static <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <div class="box-card-container box-card-container-pad-left bg-gradient-primary is-shadowed">
            <div class="row no-gutters">
                <?php if(!empty($title)): ?>
                <div class="col-sm-8 d-flex intro-col intro-col--lg">
                    <div class="box-card align-items-center">
                        <div class="box-card__body">
                            <h2 class="mb-0"><?=$title?></h2>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="col-sm-4 d-flex intro-col intro-col--sm">
                    <div class="box-card box-card--md bg-danger has-hover-action align-items-center text-white text-center">
                        <div class="box-card__body">
                            <a href="<?=$link['url']?>" class="link-stacked" <?=$target?>></a>
                            <h4 class="mb-0"><strong><?=$link['title']?></strong></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>