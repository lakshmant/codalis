<?php
/**
* https://stackoverflow.com/questions/6045607/search-for-a-string-in-a-php-file-with-a-start-and-end
*/
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$title = $block['title'];
?>
<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
       <?php if(!empty($title)) : ?> <h3 class="entry-card__title"><strong><?=$title?></strong></h3> <?php endif; ?>
        <div class="row">
            <?php
            echo '<div class="col-sm-4 mb-4 mb-sm-0">'.displayThreeColumnBlock($text_1 ).'</div>';
            echo '<div class="col-sm-4 mb-4 mb-sm-0">'.displayThreeColumnBlock($text_2).'</div>';
            echo '<div class="col-sm-4">'.displayThreeColumnBlock($text_3).'</article> </div>';
            ?>
        </div>
    </div>
</section>