<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$address = $block['address'];
?>
<section class="block block--contact <?=$spaceType?> <?=$bgColor?> is-extended">
    <div class="block__body">
        <div class="text-card-container">
            <div class="row">
            <?php if(!empty($address)): ?>
                <div class="col-sm-4 mb-4 mb-sm-0">
                    <div class="text-card text-muted">
                        <div class="text-card__body">
                            <address>
                               <?php echo $address; ?>
                            </address>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="col-sm-8">
                    <div class="form-container">
                        <?php
                            $formID = $block['select_form'];
                           $shortcode = ' [contact-form-7 id="'.$formID.'" title="Contact form 1"]';
                            echo do_shortcode($shortcode);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.Contact section ends -->
