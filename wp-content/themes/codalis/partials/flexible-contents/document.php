<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
?>
<section class="block block--utilities <?=$spaceType?> <?=$bgColor?> is-extended">
    <div class="block__body">
        <div class="utilities-card-container">
            <div class="row row-gutter-30">
                <?php
                $uploadDoc = $block['upload_documents'];
                 $docType = $block['type'];
                if($docType=="black-pdf") {
                    $iconClass = "icon-pdf-filled";
                }elseif($docType=="white-pdf"){
                    $iconClass = "icon-pdf-stroke";
                }else {
                    $iconClass = "icon-download";
                }
                $count=1;

                foreach ($uploadDoc as $doc) {
                    $uploadedFile = $doc['uploaded_file'];
                    $label = $doc['label'];
                    if($count==1) {
                        echo '<div class="col-sm-4 mb-4 mb-sm-0">
                            <div class="utilities-card">
                            <div class="utilities-card-row">';
                    }
                    ?>
					<div class="ui-btn-group mb-4">
						<a href="<?=$uploadedFile?>" class="ui-btn"><i class="icon <?=$iconClass?> icon-xl ui-btn-picto mr-0"></i><span class="ui-btn-label ml-3"><?=$label?></span></a>
					</div>
                    <?php

                    if($count==3) {
                        $count=0;
                        echo '</div></div></div>';
                    }

                    $count++;
                }
                ?>
            </div>
        </div>
    </div>
</section><!-- /.Utilities components section ends -->
