<?php
$query = new WP_Query($args);
while($query->have_posts()): $query->the_post();
    $linkedin = get_field('member_linkedln');
    $twitter = get_field('member_twitter');
    $post_thumbnail_id = get_post_thumbnail_id( $post_id );
    $imgSrc =  wp_get_attachment_image_src($post_thumbnail_id,'full');
    $imgSrc = \App\getImageManager()->resize( \App\getImageDirectoryPath($imgSrc[0]), \App\IMAGE_SIZE_TWO_COLS);
    ?>
    <div class="col-sm-4 mb-4 js-last-col col-has-mb">
        <article class="member-card">
            <figure class="member-card__picture zoom-effect-holder mb-0">
                <img alt="Article thumbnail0" class="entry-card__img img img-full" src="<?=$imgSrc?>">
                <a href="<?php the_permalink();?>" class="link-stacked"></a>
            </figure>
            <div class="member-card__body">
                <h4 class="member-card__subtitle"><a href="<?php the_permalink();?>" class="text-primary"><?php the_title(); ?></a></h4>
                <?php the_content(); ?>
            </div>
            <footer class="member-card__footer">
                <div class="social-media social-media-xs-lg">
                    <ul class="social-media__list social-media__list--inline">
                        <?php if(!empty($linkedin)) {?>
                            <li class="social-media__item"><a href="<?=$linkedin?>" class="social-media__link"><i class="icon icon-linkedin icon-primary mr-0"></i></a></li>
                        <?php }
                        if(!empty($twitter)): ?>
                            <li class="social-media__item"><a href="<?=$twitter?>" class="social-media__link"><i class="icon icon-twitter icon-primary mr-0"></i></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="ui-picto ui-picto--primary ml-auto">
                    <i class="icon icon-plus text-white mr-0"></i>
                    <a href="<?php the_permalink();?>" class="link-stacked"></a>
                </div>
            </footer>
        </article>
    </div>
<?php
endwhile;
wp_reset_postdata();