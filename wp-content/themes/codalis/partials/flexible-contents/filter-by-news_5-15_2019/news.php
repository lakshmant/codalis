<?php
$spaceType = empty($block['space_type'])?'spacing-py-md':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
?>
<style>
    .spinner-category-wrap{
        position: relative;
    }
    .spinner-category{
        position: absolute; top: 0; left: 50%; transform: translateX(-50%);
    }
    .spinner-category img{
        width: 80px !important;
    }
</style>
<div class="block block--filter <?=$spaceType?> <?=$bgColor?> is-extended">
    <div class="block__body">
        <nav class="filter-nav" role="navigation">
            <div class="filter-nav__label is-collapsed js-collapse-xs-toggle" data-panel-toggle="js-collapse-xs-panel">Filtrer par</div>
            <form name="filter-form" class="form-filter" id="js-form">
                <ul class="filter-nav__list js-collapse-xs-panel" role="listbox">
                    <?php
                    $categories = get_categories( array(
                        'taxonomy' => 'category',
                        'orderby' => 'name',
                        'order'   => 'ASC'
                    ) );
                    foreach ($categories as $cat):
                        $termId = $cat->term_id;
                        $termName = $cat->name;
                        ?>
                        <li class="filter-nav__item">
                            <input class="d-none filter-field-input js-filter-checkbox" id="category<?=$termId?>" name="category[]" type="checkbox" value="<?=$termId?>">
                            <label class="btn filter-field-label" for="category<?=$termId?>"><?=$termName?></label>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </form>
        </nav>
        <div class="spinner-category-wrap">
            <div id="loading-image" class="text-center spinner-category" >
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ajax-loader.svg" />
            </div>
        </div>
        <!-- Script to add toggle feature in responsive mode -->
        <script>
            ;(function() {
                var collapseToggle = document.querySelector('.js-collapse-xs-toggle');
                var collapseTarget = '.' + collapseToggle.getAttribute('data-panel-toggle');

                if (collapseToggle) {
                    collapseToggle.addEventListener('click', function(e) {

                        if (collapseToggle.className.indexOf('is-collapsed') !== -1) {
                            collapseToggle.classList.remove('is-collapsed');
                            document.querySelector(collapseTarget).classList.add('is-shown');
                        } else {
                            collapseToggle.classList.add('is-collapsed');
                            document.querySelector(collapseTarget).classList.remove('is-shown');
                        }
                        e.preventDefault();
                    });
                }

                var checkboxes = document.querySelectorAll('.js-filter-checkbox');

                for (var i = 0, j = checkboxes.length; i < j; i++) {
                    checkboxes[i].addEventListener('change', function() {
                        submitForm();
                    });
                }

            })();
        </script>

    </div>
</div>


<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <!-- Social media / Square card group -->
        <div class="square-card-container">
            <div class="row js-row js-news">
            </div>
            <div class="col-12 not-found-news h3 text-danger text-center d-none" style="padding-bottom: 50px;"> <?php  _e("Content does not match the filter criteria","app"); ?></div>
        </div><!-- /.Social media / Square card group ends -->
    </div>
    <?php
  /*  $newsTags = $block['display_tags'];
    if(!empty($newsTags)):
        $newsTags = implode(',',$newsTags);
        if(!empty($newsTags)) :
            $jaxTag = "tag=".$newsTags."&";
        else:
            $jaxTag ='';
        endif;
    endif;*/
    ?>
    <script>
        function submitForm() {

            var selectedCategories = [];
            var catInput ="" ;
            var filterCheckbox =   document.querySelectorAll(".js-filter-tag-checkbox");
            if(filterCheckbox.length > 0) {
                [].forEach.call(filterCheckbox, function (item) {
                    if (item.checked) selectedCategories.push(item.value);
                });
                if (selectedCategories.length > 0) {
                    catInput = selectedCategories.join(",");
                }
            }

            var frmData = 'cat='+catInput +'&action=display_contents';
            var ajaxURL = "<?=admin_url('admin-ajax.php')?>";
            $.ajax({
                url: ajaxURL,
                data: frmData,
                beforeSend: function(){
                    // Show image container
                    $("#loading-image").show();
                },
                complete: function(){
                    $('#loading-image').hide();
                },
                success: function(response){
                    //console.log('Response length is '+response.length);
                    if (response.length > 0) {
                        $(".not-found-news").addClass("d-none");
                     //  $(".js-news").show();
                        $(".js-news").html(response);
                        // == Renit javascript codes related to these blocks
                        window.buildLightBoxVideo($('.js-lightbox')); // == Referenced from ps-script.js
                        window.getLastRowCols($('.js-news'), $('.js-last-col')); // == Referenced from ps-script.js
                    }else {
                     //   $('.js-news').css({"display" : "none"} );
                        $(".not-found-news").removeClass("d-none");
                        $(".js-news").hide();
                    }
                }

            });
        }
        jQuery( document ).ready(function() {
            submitForm();
        });
    </script>
</section>
