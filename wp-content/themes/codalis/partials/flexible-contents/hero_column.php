<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/27/2018
 * Time: 1:47 PM
 */
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$title = $block['title'];
$text = $block['text'];
$image1 = $block['image'];
$image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image1), \App\IMAGE_SIZE_SERVICE);

$text2 = $block['text_2'];
$image2 = $block['image_2'];
$image2 = \App\getImageManager()->resize( \App\getImageDirectoryPath($image2), \App\IMAGE_SIZE_SERVICE);
?>

<section class="stacked-block <?=$bgColor?> <?=$spaceType?> is-extended">
    <?php if(!empty($title)): ?>
        <header class="stacked-block__header">
            <h2 class="stacked-block__title has-line right"><?=$title?></h2>
        </header>
    <?php endif; ?>

    <div class="stacked-block__body">
        <?php if(!empty($image1) || !empty($text)) : ?>
        <div class="stacked-card-container stacked-card-container--child-pushed align-items-center">
            <div class="stacked-card-col stacked-card-col--picture">
                <div class="stacked-card">
                    <div class="stacked-card__body p-0">
                        <figure class="stacked-card__picture mb-0">
                            <img alt="Image Alt" class="stacked-card__img img"  src="<?=$image1?>">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="stacked-card-col stacked-card-col--text">
                <div class="stacked-card stacked-card--xs is-shadowed bg-white">
                    <div class="stacked-card__body">
                      <?php
                      $text = explode(" ",$text);
                      $text = str_replace('<h1>', '<h1 class="overlay-card__title text-white mb-1">', $text);    // baseline single quote
                      $text = str_replace('<h3>', '<h3 class="stacked-card__title">', $text);    // baseline single quote
                      $text = implode(' ',$text);
                      echo $text;
                      ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if(!empty($image2) || !empty($text2)) : ?>
        <div class="stacked-card-container stacked-card-container--child-pushed">
            <div class="stacked-card-col stacked-card-col--picture order-sm-2">
                <div class="stacked-card">
                    <div class="stacked-card__body p-0">
                        <figure class="stacked-card__picture mb-0">
                            <img alt="Image Alt" class="stacked-card__img img" src="<?=$image2?>">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="stacked-card-col stacked-card-col--text mb-pushed-135">
                <div class="stacked-card stacked-card--xs is-shadowed bg-white">
                    <div class="stacked-card__body">
                        <?php
                        $text2 = explode(" ",$text2);
                        $text2 = str_replace('<h1>', '<h1 class="overlay-card__title text-white mb-1">', $text2);    // baseline single quote
                        $text2 = str_replace('<h3>', '<h3 class="stacked-card__title">', $text2);    // baseline single quote
                        $text2 = implode(' ',$text2);
                        echo $text2;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
