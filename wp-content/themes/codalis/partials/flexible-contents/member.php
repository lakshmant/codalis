<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$members = $block['select_members'];
$title = $block['title'];
$query = new WP_Query( array( 'post_type' => 'member', 'post__in'=> $members, 'suppress_filters'=> false, 'post_status' => 'publish' ) );

include 'member/member-list.php';