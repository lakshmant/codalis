<?php
$spaceType = empty($block['space_type'])?'spacing-py-md':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];

$contentType = ['post','case-study'];
$args = ['post_type' => $contentType,'posts_per_page'=>3,  'suppress_filters'  =>  false, 'post_status'=>'publish'];
$newsTags = $block['display_tags'];
if(!empty($newsTags)):
    $termId = implode(',',$newsTags);
    $args['tag__in'] = $newsTags;
endif;
$title = $block['title'];

?>
<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <?php if(!empty($title)): ?>
        <header class="block__header">
            <h2 class="stacked-block__title"><?=$title?></h2>
        </header>
    <?php endif; ?>
    <div class="block__body">
        <div class="square-card-container">
            <div class="row js-row js-case-study">
                <?php include 'filter-by-news/news-casestudy-items.php'; ?>
            </div>
        </div>
    </div>
</section>