<?php
    $mainTitle = $block['main_title'];
    $subTitle = $block['subtitle'];
    $images = $block['images'];
    $countImage = count($block['images']);
if(!empty($images)):
?>

<section class="stacked-block stacked-block--mb bg-secondary text-faded spacing-py-eq is-extended" >
    <div class="stacked-block__body">
        <div class="stacked-card-container counter-slider-parent js-counter-slider-parent">
            <div class="stacked-card-col stacked-card-col--text mb-4 mb-sm-0">
                <div class="stacked-card stacked-card--xs">
                    <div class="stacked-card__body pb-0 pl-0 p-xs-0">
                        <?php if(!empty($mainTitle)): ?>
                            <h4 class="stacked-card__subtitle text-primary"><?=$mainTitle?></h4>
                        <?php endif; ?>
                        <?php if(!empty($subTitle)): ?>
                            <h3 class="stacked-block__title has-line thin bottom"><?=$subTitle?></span></h3>
                        <?php endif; ?>
                        <div class="counter-controls bottom position-relative">
                            <div class="swiper-button-prev js-counter-swiper-button-prev"><i class="icon icon-left-chev icon-white mr-0"></i></div>
                            <div class="swiper-button-next js-counter-swiper-button-next"><i class="icon icon-right-chev icon-white mr-0"></i></div>
                            <div class="counter-slider__count js-counter-slider-count"><span class="js-current-slide current-slide-node text-primary">1</span>&nbsp;&nbsp;/&nbsp;&nbsp;<span class="js-total-slide"><?=$countImage?></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="stacked-card-col stacked-card-col--picture mb-pushed-90">
                <div class="stacked-card is-shadowed">
                    <div class="stacked-card__body p-0">

                        <div class="swiper-container counter-slider js-counter-slider">
                            <div class="swiper-wrapper">
                                <?php foreach ($images as $image):
                                    $img = $image['image'];
                                    $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($img), \App\IMAGE_SIZE_SERVICE);
                                    ?>
                                <div class="swiper-slide counter-slider__item">
                                    <figure class="counter-slider-picture text-center mb-0"><img alt="Partner1 Logo" class="img" src="<?php echo $img; ?>" /> </figure>
                                </div>
                                <?php endforeach;?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;