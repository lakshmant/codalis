<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$table = $block['table'];
if(!empty($table)):
?>

<section class="block block--table <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <table class="table table--striped table--responsive text-center">
            <?php
                if ( $table['header'] ) {
                    echo '<thead>';
                    echo '<tr>';
                    foreach ( $table['header'] as $th ) {
                        echo '<th><h4>';
                        echo $th['c'];
                        echo '</h4></th>';
                    }
                    echo '</tr>';
                    echo '</thead>';
                }
                echo '<tbody>';
                foreach ( $table['body'] as $tr ) {
                    echo '<tr>';
                    foreach ( $tr as $td ) {
                        echo '<td data-th-title="'. $td['c'].'">';
                        echo $td['c'];
                        echo '</td>';
                    }
                    echo '</tr>';
                }
                echo '</tbody>';
            ?>
        </table>
    </div>
</section>
<?php endif;