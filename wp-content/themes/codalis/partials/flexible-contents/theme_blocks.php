<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$contents = $block['contents'];
?>
<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <!-- Social media / Square card group -->
        <div class="square-card-container">
            <div class="row js-row">
                <?php
                if(!empty($contents)):
                    foreach ($contents as $content) {
                   $type = $content['type'];
                    $image = $content['image'];
                    $image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_THEMES_BLOCK);
                    $link = $content['link'];
                    $hoverText = $content['image_hover_text'];
                    $date = $content['date'];
                    $label = $content['label'];
                    $title = $content['title'];
                   // $text = $content['text'];
                    if($type=="video") :
                        $class ="video-card";
                    elseif (($type=='event')):
                        $class ="event-card ";
                    else :
                        $class ="study-case-card ";
                    endif;

                    echo '<div class="col-sm-4 col-xmd-6 squared-card-col mb-4 js-last-col col-has-mb">
                                                <article class="square-card '.$class.' has-hover-action">
                                                 <div class="square-card-inner hover-effect-bubba text-white">
                                    <figure class="square-card__picture zoom-effect-holder mb-0">
                                        <img alt="Event Image" class="square-card__img img img-full " src="'.$image.'">
                                    </figure>';

                    if($type=="video") :
                        $youTubeVideoID = app\youtube_video_id($link);
                        ?>
                                    <footer class="square-card__footer right text-right">
                                        <div class="ui-picto ui-picto--lg ui-picto--info">
                                            <div class="ui-picto__row">
                                                <div class="ui-picto__item">
                                                    <i class="icon icon-play-outline icon-lg mr-0"></i>
                                                </div>
                                                <?php if(!empty($label)): ?>
                                                <div class="ui-picto__item">
                                                    <div class="h5 mb-0"><?=$label?></div>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </footer>
                                    <div class="square-card__hover-content">
                                        <div class="square-card__hover-content-inner">
                                            <?php if(!empty($title)): ?>
                                                <div class="mb-2"><?=$title?></div>
                                            <?php endif; ?>
                                            <?php if(!empty($hoverText)) :
                                                        echo $hoverText;
                                                endif;
                                            ?>
                                            <div class="js-lightbox">
                                                <a href="https://www.youtube.com/embed/<?=$youTubeVideoID?>" class="link-stacked js-lightbox-play"></a>
                                            </div>
                                        </div>
                                    </div>
                    <?php elseif ($type=="event"): ?>
                                    <footer class="square-card__footer right text-right">
                                        <div class="ui-picto ui-picto--lg ui-picto--success">
                                            <div class="ui-picto__row">
                                                <div class="ui-picto__item">
                                                    <i class="icon icon-calendar icon-lg mr-0"></i>
                                                </div>
                                                <div class="ui-picto__item">
                                                    <div class="h3 mb-0 event-date"><strong><?php //echo $date->format('j'); ?></strong></div>
                                                    <div class="event-month"><?php  //echo $date->format('M'); ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                    <div class="square-card__hover-content">
                                        <div class="square-card__hover-content-inner">
                                            <?php if(!empty($title)): ?>
                                                <div class="mb-2"><?=$title?></div>
                                            <?php endif; ?>
                                            <?php if(!empty($hoverText)) :
                                                echo $hoverText;
                                            endif;
                                            ?>
                                            <a href="<?=$link?>" class="link-stacked"></a>
                                        </div>
                                    </div>

                <?php elseif ($type=="study-case"):?>
                                    <footer class="square-card__footer right text-right">
                                        <div class="ui-picto ui-picto--lg ui-picto--danger">
                                            <div class="ui-picto__row">
                                                <div class="ui-picto__item">
                                                    <i class="icon icon-docs icon-lg mr-0"></i>
                                                </div>
                                                <div class="ui-picto__item">
                                                    <div class="h5 mb-0"><?=$label?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                    <div class="square-card__hover-content">
                                        <div class="square-card__hover-content-inner">
                                            <?php if(!empty($title)): ?>
                                                <div class="mb-2"><?=$title?></div>
                                            <?php endif; ?>
                                            <?php if(!empty($hoverText)) :
                                                echo $hoverText;
                                            endif;
                                            ?>
                                            <a href="<?=$link?>" class="link-stacked"></a>
                                        </div>
                                    </div>
                        <?php
                    else: ?>
                        <div class="square-card__hover-content">
                            <div class="square-card__hover-content-inner">
                                <?php if(!empty($title)): ?>
                                    <div class="mb-2"><?=$title?></div>
                                <?php endif; ?>
                                <?php if(!empty($hoverText)) :
                                    echo $hoverText;
                                endif;
                                ?>
                                <a href="<?=$link?>" class="link-stacked"></a>
                            </div>
                        </div>
                <?php
                    endif;
                    echo ' </div></article></div>';
                }
                endif;
                ?>

            </div>
        </div><!-- /.Social media / Square card group ends -->
    </div>
</section>