<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/20/2018
 * Time: 12:04 PM
 */

$type = $block['type'];
$spaceType = $block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$image = $block['image'];
$image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_VIDEO_BANNER_CMS);
$youtubeVideo = $block['youtube_video'];
$youTubeVideoID = app\youtube_video_id($youtubeVideo);

if($type=="full-width-video"):
    if(!empty($youTubeVideoID)):
?>
<section class="block block--video <?=$bgColor?> <?=$spaceType?> is-extended-full">
    <div class="block__body">
        <div class="lightbox-video-card position-relative">
            <?php if(!empty($image)): ?>
                <figure class="lightbox-video-card__thumb mb-0">
                    <img alt="Video Thumbnail" class="img img-full" src="<?=$image?>">
                </figure>
            <?php endif; ?>


            <div class="lightbox-video-card__content overlay-card center">
                <div class="js-lightbox">
                    <a href="https://www.youtube.com/embed/<?=$youTubeVideoID?>" class="js-lightbox-play" target="_blank"><i class="icon icon-play icon-xlg icon-faded mr-0"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
    <?php endif; ?>
<?php else :
    if(!empty($youTubeVideoID)):
?>
    <section class="block block--video <?=$bgColor?> <?=$spaceType?> text-center is-extended">
        <div class="block__body">
            <div class="video-container">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?=$youTubeVideoID?>?autoplay=0;rel=0;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </section>
<?php endif;
endif;