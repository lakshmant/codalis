<?php
/*
 * only meta head info
 */
?>
<head>
    <meta charset="<?php bloginfo( 'charset' );?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- Place favicon.ico in the favicon directory -->
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Import icomoon font library here to avoid broken icon issue while using cache plugin (it sucks) -->
    <style>
        @font-face {
            font-family: 'icomoon';
            src:  url('<?= get_template_directory_uri(); ?>/assets/fonts/icomoon/icomoon.eot?v96ucb');
            src:  url('<?= get_template_directory_uri(); ?>/assets/fonts/icomoon/icomoon.eot?v96ucb#iefix') format('embedded-opentype'),
            url('<?= get_template_directory_uri(); ?>/assets/fonts/icomoon/icomoon.ttf?v96ucb') format('truetype'),
            url('<?= get_template_directory_uri(); ?>/assets/fonts/icomoon/icomoon.woff?v96ucb') format('woff'),
            url('<?= get_template_directory_uri(); ?>/assets/fonts/icomoon/icomoon.svg?v96ucb#icomoon') format('svg');
            font-weight: normal;
            font-style: normal;
        }
    </style>

    <?php  wp_head(); ?>
</head>
