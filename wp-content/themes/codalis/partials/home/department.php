<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/15/2018
 * Time: 12:48 PM
 */

$member = get_field('member_name');
//$image = get_field('member_image');
$departmentCategories = get_field('department_categories');
$departmentCategoriesPop = $departmentCategories[0];
$image = $departmentCategoriesPop['image'];

$image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_SERVICE);

?>
<section class="stacked-block bg-secondary text-faded spacing-py-eq is-extended">
    <div class="stacked-block__body">
        <div class="stacked-card-container js-stacked-card-container">
            <?php  if(!empty($image) || !empty($member)):?>
                <div class="stacked-card-col stacked-card-col--picture order-sm-2 mb-pushed-90 mb-sm-0 mb-4">
                    <div class="stacked-card">
                        <div class="stacked-card__body p-0">
                            <?php if(!empty($image)):?>
                            <figure class="stacked-card__picture has-cover mb-0 js-stacked-card-picture" style="background-image:url(<?=$image?>);">
                              <?php endif; ?>
                                <?php if(!empty($member)) :?>
                                <figcaption class="stacked-card__picture-caption overlay-card bottom js-stacked-card-picture-caption text-faded"><?=$member?></figcaption>
                                <?php endif; ?>
                            </figure>
                        </div>
                    </div>
                </div>
            <?php endif;


            $membersLink = get_field('all_members_link');
            if(!empty($departmentCategories) || !empty($membersLink)):
            ?>
            <div class="stacked-card-col stacked-card-col--text">
                <div class="stacked-block__header">
                    <?php
                        $title = get_field('department_title');
                        $title = explode(' ',$title);
                    $args = array(
                        'post_type'  =>  'member',
                        'suppress_filters'  =>  false,
                        "post_status"  =>  'publish'
                    );
                    $query = new WP_Query( $args );
                    $memberCount = $query->found_posts;
                        echo '<h2 class="stacked-block__title has-line bottom">'.$title[0].' <strong>'. $memberCount.'</strong> <br/>'.'<span class="text-faded">'. $title[1].'</span></h2>';
                    ?>
                </div>
                <div class="stacked-card">
                    <div class="stacked-card__body pl-0 py-0">
                        <?php if(!empty($departmentCategories)): ?>
                            <ul class="lists lists--arrow lists-faded stacked-card__list js-synced-lists mb-sm-4">
                                <?php
                                $c=1;
                                foreach ($departmentCategories as $category):
                                    $cat = $category['categories'];
                                    $target = !empty($cat['target'])?'target="_blank"':'';

                                    $active = ($c==1)?'is-active':'';
                                    ?>
                                <li class="lists__item <?=$active?>">
                                    <a href="<?=$cat['url']?>" <?=$target?> class="lists__link js-synced-lists-link" data-img-src="<?php echo $category['image']; ?>"><?=$cat['title']?></a>
                                </li>
                                <?php $c++; endforeach; ?>
                                <?php //endif; ?>
                            </ul>
                        <?php
                        endif;
                        if(!empty($membersLink)):
                            if(!empty($membersLink['target'])) :
                                $target='target="_blank"';
                            endif;
                        ?>
                        <a href="<?=$membersLink['url']?>" class="btn-link btn-link-faded" role="button" <?=$target?>><u><?=$membersLink['title']?></u></a>
                        <?php  endif;?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section><!-- /.Home page members section ends -->

