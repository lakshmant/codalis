<?php
$presentationtText = get_field('pesentation_text');
$keyNumbers = get_field('key_numbers');
if(!empty($presentationtText) || !empty($keyNumbers)):
?>
<section class="block block--presentation is-extended">
    <div class="block__body">
        <div class="box-card-container box-card-container-pad-left bg-white is-shadowed text-lg fw-l is-xs-extended">
            <div class="row no-gutters">
                <?php
                if(!empty($presentationtText)):
                ?>
                <div class="col-sm-4 d-flex">
                    <div class="box-card box-card--md align-items-center">
                        <div class="box-card__body pr-sm-0">
                            <?=$presentationtText?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="col-sm-8 d-flex">
                    <div class="box-card box-card--md align-items-center text-center">
                        <div class="box-card__body">
                            <div class="row grid-v-border-separator">
                                <?php
                                $count=1;
                                $class='';
                                foreach ($keyNumbers as $num) :
                                $class = ($count<3) ? 'mb-3 mb-sm-0' : '';
                                ?>
                                <div class="col-4 d-flex grid-col-styled">
                                    <div class="counter-card <?=$class?>">
                                        <div class="counter-card__title h2 mb-0"><strong><?=$num['number']?></strong></div>
                                        <?php if(!empty($num['text'])) : ?> <p><?=$num['text']?></p> <?php endif; ?>
                                    </div>
                                </div>
                                <?php $count++; endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;