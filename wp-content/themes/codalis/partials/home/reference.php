<?php
$logos = get_field("reference_logos");
if(!empty($logos)):
?>
<section class="block bg-white spacing-py-eq is-extended">
    <?php
    $title = get_field('reference_title');
    if(!empty($title)):
        ?>
        <header class="block__header">
            <h2 class="block__title"><?=$title?></span></h2>
        </header>
    <?php endif; ?>

    <div class="block__body">
        <div class="counter-carousel-parent js-counter-carousel-parent mt-5 mt-sm-6 mt-md-7">
            <div class="swiper-container counter-carousel js-counter-carousel" data-breakpoints-slides-number='{"desktop": 6, "mobile_portrait": 4, "mobile": 3}'>
                <div class="swiper-wrapper">
                    <?php
                    $countLogo = count($logos);
                    foreach ($logos as $logo):
                        if(!empty($logo['logo'])):
                            ?>
                            <div class="swiper-slide counter-slider__item">
                                <figure class="counter-slider-picture grayscale-filter-holder text-center mb-0">
                                    <a href="<?=$logo['url']?>"><img alt="Partner1 Logo" class="img grayscale-filter-media js-grayscale-filter-media" src="<?=$logo['logo']?>" /></a>
                                </figure>
                            </div>
                    <?php endif; endforeach; ?>
                </div>
            </div>
            <div class="counter-controls top-right">
                <div class="swiper-button-prev js-counter-swiper-button-prev"><strong><i class="icon icon-left-chev icon-white mr-0"></i></strong></div>
                <div class="swiper-button-next js-counter-swiper-button-next"><strong><i class="icon icon-right-chev icon-white mr-0"></i></strong></div>
                <div class="counter-slider__count js-counter-slider-count"><span class="js-current-slide current-slide-node">1</span>&nbsp;&nbsp;/&nbsp;&nbsp;<span class="js-total-slide"><?=$countLogo?></span></div>
            </div>
        </div>
    </div>
</section><!-- /.Home page our clients section ends -->
<?php endif;
