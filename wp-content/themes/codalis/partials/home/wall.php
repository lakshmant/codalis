<section class="block spacing-py-eq bg-faded is-extended">
    <?php
     $wallTitle = !empty(get_field('wall_title'))?get_field('wall_title') :'';
     if(!empty($wallTitle)) :
    ?>
    <header class="block__header">
        <h2 class="block__title has-line has-line--lg right"><?=$wallTitle?></span></h2>
    </header>
    <?php endif; ?>
    <div class="block__body">
        <div class="square-card-container">
            <div class="row square-card-row">
                <div class="col-sm-4 col-xmd-6 squared-card-col mb-4">
                    <article class="square-card twitter-card bg-gradient-primary">
                        <?php
                        $twitter = !empty(get_field('twitter_link'))?get_field('twitter_link'):'';
                        $target ="";
                        if(!empty($twitter['target'])):
                            $target='target="_blank"';
                        endif;

                        if(!empty($twitter)):
                        ?>
                        <a href="<?=$twitter['url']?>"  <?=$target?> class="square-card__link link-stacked"></a>
                        <?php endif; ?>
                        <div class="square-card__body">
                            <div class="twitter-card-content">
                                <?php // fetchTweets(count="10") ;
                                use GuzzleHttp\Client;
                                use GuzzleHttp\Exception\RequestException;
                                use GuzzleHttp\HandlerStack;
                                use GuzzleHttp\Subscriber\Oauth\Oauth1;

                             //   require 'vendor/autoload.php';

                                $consumerID = 'M2qkK69IWFtUwRQJKBw';
                                $consumerSecret = 'iG9Pc56qqCHDbLZvCosIFKf8K36FilKAfGVjOsqaM';

                                $token = '290551724-3s9neUGvW83ybbNids1oKpUdHBHrxmi1OJThNELU';
                                $secret = 'MFevpP75yYJ5wqs6Nx6OXW4q8xkYzpY3XOydYWCBbg';

                                $source = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=twitterapi&count=2";

                                $stack = HandlerStack::create();
                                $middleware = new Oauth1([
                                    'consumer_key'    => $consumerID,
                                    'consumer_secret' => $consumerSecret,
                                    'token'           => $token,
                                    'token_secret'    => $secret
                                ]);

                                $stack->push($middleware);

                                $client = new Client([
                                    'base_uri' => 'https://api.twitter.com/1.1/',
                                    'handler' => $stack,
                                    'auth' => 'oauth',
                                    'verify' => false
                                ]);

                                try{
                                    $response = $client->get('statuses/user_timeline.json',['query' => [
                                        'screen_name' => 'Codalis',
                                        'count' => '1',
                                    ]]);

                                    $tweets = json_decode($response->getBody()->getContents());

                                    function findLinkInString($string){
                                        $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
                                        $string = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $string);
                                        return $string;
                                    }

                                    foreach ($tweets as $tweet){
                                        //echo "<p><a target='_blank' href='http://twitter.com/Codalis/status/{$tweet->id}'>".findLinkInString($tweet->text).'</a></p>';
                                        echo "<p>".substr($tweet->text,0,150).'</p>';
                                    }
                                }catch (RequestException $e){
                                    //var_dump($e->getMessage());
                                }
                                ?>
                                <?php //echo do_shortcode('[fetch_tweets id="163"]') ?>
                                <?php /*if(!empty($twitterTitle)): */?><!-- <div class="twitter-tag mb-2"><?/*=$twitterTitle*/?></div> --><?php /*endif;*/ ?>
                                    <?php /*
                                <?php if(!empty($twitterText)): ?>
                                        <p><?=$twitterText?></p>
                                <?php endif; ?>
 */ ?>
                            </div>
                        </div>
                        <footer class="square-card__footer square-card__footer--lg">
                            <div class="social-media line-height-1">
                                <button class="social-media__link d-flex text-left align-items-center text-white"><i class="icon icon-twitter icon-lg mr-2"></i>
                                    <?php
                                    if(!empty($twitter)):
                                        ?>
                                        <span><?=$twitter['title']?></span>
                                    <?php endif; ?>
                                </button>
                            </div>
                        </footer>
                    </article>
                </div>

                <div class="col-sm-4 col-xmd-6 squared-card-col mb-4">
                    <article class="square-card instagram-card">
                        <?php
                        $instagram = !empty(get_field('instagram_link'))?get_field('instagram_link'):'';
                        $target ="";
                        if(!empty($instagram['target'])):
                            $target='target="_blank"';
                        endif;

                        if(!empty($instagram)):
                        ?>
                        <a href="<?=$instagram['url']?>"  <?=$target?> class="square-card__link link-stacked"></a>
                        <?php endif; ?>
                        <?php
                        $instaItems = \app\getInstagramFeeds(1);
                        //var_dump($instaItems);
                       if(!empty($instaItems)):
                        foreach($instaItems as $instaItem):
                            $link = $instaItem[0];
                            $image = $instaItem[1];
                            $image = str_replace("http://","https://", $image);
                            ?>

                            <figure class="square-card__picture zoom-effect-holder mb-0">
                                <img alt="Instagram Image" class="square-card__img img img-full zoom-effect" src="<?php echo $image; ?>" />
                            </figure>

                        <?php endforeach; endif; ?>



                        <footer class="square-card__footer square-card__footer--lg">
                            <div class="social-media line-height-1">
                                <button class="social-media__link d-flex align-items-center text-left text-white"><i class="icon icon-instagram icon-lg mr-2"></i>
                                    <?php
                                    if(!empty($instagram)):
                                    ?>
                                    <span><?=$instagram['title']?></span>
                                    <?php endif; ?>
                                </button>
                            </div>
                        </footer>
                    </article>
                </div>

                <?php
                 $event= !empty(get_field('event'))?get_field('event'):'';



                if(!empty($event)):

                   $hideDate = !empty($event["hide_date"])?$event["hide_date"]:'';
                ?>
                <div class="col-sm-4 col-xmd-6 squared-card-col mb-4">
                    <article class="square-card event-card has-hover-action">
                        <div class="square-card-inner hover-effect-bubba text-white">
                            <?php
                            $eventImage = $event['image']['url'];
                            $eventImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($eventImage), \App\IMAGE_SIZE_EVENT);
                            if(!empty($eventImage) || $eventImage=='null'): ?>
                            <figure class="square-card__picture zoom-effect-holder mb-0">
                                <img alt="Event Image" class="square-card__img img img-full zoom-effect" src="<?php echo $eventImage; ?>" />
                            </figure>
                            <?php endif;?>


                        <?php if(!empty($event['link'])): ?>
                            <footer class="square-card__footer square-card__footer--lg">
                                <div class="ui-picto ui-picto--faded">
                                    <i class="icon icon-plus text-primary mr-0"></i>
                                    <a href="<?php echo $event['link']; ?>" class="link-stacked"></a>
                                </div>
                            </footer>
                            <?php endif; ?>


                            <?php if(empty($hideDate)):
                                
                                $dateformatDay = "d";
                                $dateformatMonth = "M";
                                $date = $event['date'];
                                $eventDate = explode(' ',$date);

                                $month = $eventDate[0];
                                $day = $eventDate[1];
                                $year  = $eventDate[2];
                                $day = preg_replace('/[ ,]+/', '', $day);

                                ?>
                                <footer class="square-card__footer right text-right">
                                    <div class="ui-picto ui-picto--lg ui-picto--primary">
                                        <div class="ui-picto__row">
                                            <div class="ui-picto__item">
                                                <i class="icon icon-calendar icon-lg mr-0"></i>
                                            </div>
                                            <div class="ui-picto__item">
                                                <div class="h3 mb-0 event-date"><strong><?php echo $day; ?></strong></div>
                                                <div class="event-month"><?php echo $month; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                            <?php endif; ?>

                            <?php if(!empty($event['description']) || !empty($event['title'])): ?>
                            <div class="square-card__hover-content">
                                <div class="square-card__hover-content-inner">
                                    <div class="mb-2"><?=$event['title']?></div>
                                    <?php echo $event['description']; ?>
                                    <a href="<?=$event['link']?>" class="link-stacked d-none"></a>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </article>
                </div>
                <?php endif;
                $partner = !empty(get_field('partners'))?get_field('partners'):get_field('partners');
                $partnerLogo = !empty($partner['logo'])?$partner['logo']:'';

                if(!empty($partnerLogo)):
                        $countPartner = count($partner['logo']);
                ?>
                <div class="col-sm-4 col-xmd-6 squared-card-col mb-sm-0 mb-4">
                    <article class="square-card logo-card bg-info text-white js-counter-slider-parent">
                        <div class="square-card__body">
                            <?php if(!empty($partner['title'])): ?>
                                <h4 class="logo-card__title text-normal mb-4"><strong><?=$partner['title']?></strong></h4>
                            <?php endif; ?>
                            <div class="swiper-container counter-slider js-counter-slider">
                                <div class="swiper-wrapper">
                                    <?php foreach ($partnerLogo as $pLogo): ?>
                                    <div class="swiper-slide counter-slider__item">
                                        <?php if(!empty($partner['link'])): ?>
                                                <a href="<?=$partner["link"]?>">
                                                    <figure class="counter-slider-picture text-center mb-0"><img alt="Partner Logo" class="img" src="<?php echo $pLogo['image']; ?>" /> </figure>
                                                </a>
                                        <?php else : ?>
                                            <figure class="counter-slider-picture text-center mb-0"><img alt="Partner Logo" class="img" src="<?php echo $pLogo['image']; ?>" /> </figure>
                                        <?php  endif;?>

                                    </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                        <footer class="square-card__footer square-card__footer--lg right text-right">
                            <div class="counter-controls">
                                <div class="swiper-button-prev js-counter-swiper-button-prev"><i class="icon icon-left-chev icon-white mr-0"></i></div>
                                <div class="swiper-button-next js-counter-swiper-button-next"><i class="icon icon-right-chev icon-white mr-0"></i></div>
                                <div class="counter-slider__count js-counter-slider-count"><span class="js-current-slide current-slide-node">1</span>&nbsp;&nbsp;/&nbsp;&nbsp;<span class="js-total-slide"><?=$countPartner?></span></div>
                            </div>

                            <?php /*
 //Hidden for a moment
 if(!empty($partner)): */?><!--
                            <div class="ui-picto ui-picto--faded">
                                <i class="icon icon-plus text-info mr-0"></i>
                                <a href="<?/*=$partner['link']*/?>" class="link-stacked"></a>
                            </div>
                            --><?php /*endif;*/ ?>
                        </footer>
                    </article>
                </div>
                <?php endif;
                $client = get_field('clients');
                $clientLink = !empty($client["link"])?$client["link"] : "#";
                $clientLogo = !empty($client['logo'])?$client['logo']:'';
                if(!empty($clientLogo)):
                $countClient = count($client['logo']);
                ?>

                <div class="col-sm-4 col-xmd-6 squared-card-col mb-sm-0 mb-4">
                    <article class="square-card logo-card bg-gradient-primary text-white js-counter-slider-parent">
                        <div class="square-card__body">
                            <?php if(!empty($client['title'])): ?>
                                <h4 class="logo-card__title text-normal mb-4"><strong><?=$client['title']?></strong></h4>
                            <?php endif; ?>
                            <div class="swiper-container counter-slider js-counter-slider">
                                <div class="swiper-wrapper">
                            <?php foreach ($clientLogo as $cLogo): ?>
                                    <div class="swiper-slide counter-slider__item">
                                        <?php if(!empty($clientLink)): ?>
                                                <a href="<?=$clientLink?>">
                                                    <figure class="counter-slider-picture text-center mb-0"><img alt="Partner1 Logo" class="img" src="<?php echo $cLogo['image']; ?>" /> </figure>
                                                </a>
                                        <?php else : ?>
                                                <figure class="counter-slider-picture text-center mb-0"><img alt="Partner1 Logo" class="img" src="<?php echo $cLogo['image']; ?>" /> </figure>
                                        <?php endif; ?>
                                    </div>
                            <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <footer class="square-card__footer square-card__footer--lg right text-right">
                            <div class="counter-controls">
                                <div class="swiper-button-prev js-counter-swiper-button-prev"><i class="icon icon-left-chev icon-white mr-0"></i></div>
                                <div class="swiper-button-next js-counter-swiper-button-next"><i class="icon icon-right-chev icon-white mr-0"></i></div>
                                <div class="counter-slider__count js-counter-slider-count"><span class="js-current-slide current-slide-node">1</span>&nbsp;&nbsp;/&nbsp;&nbsp;<span class="js-total-slide"><?php echo $countClient; ?></span></div>
                            </div>
                            <?php /*
     //Hidden for a moment
 if(!empty($clientLink)): */?><!--
                            <div class="ui-picto ui-picto--faded">
                                <i class="icon icon-plus text-success mr-0"></i>
                                <a href="<?/*=$clientLink*/?>" class="link-stacked"></a>
                            </div>
                            --><?php /*endif; */?>
                        </footer>
                    </article>
                </div>
                <?php endif;
                $blog = !empty(get_field('blog'))?get_field('blog'):'';
                if(!empty($blog)):
                ?>
                <div class="col-sm-4 col-xmd-6 squared-card-col">
                    <article class="square-card event-card has-hover-action">
                        <div class="square-card-inner hover-effect-bubba text-white">
                            <?php if(!empty($blog['image'])): ?>
                            <figure class="square-card__picture zoom-effect-holder mb-0">
                                <img alt="Event Image" class="square-card__img img img-full zoom-effect" src="<?php echo $blog['image']; ?>" />
                            </figure>
                            <?php endif; ?>

                            <?php if(!empty($blog['link'])):?>
                                <footer class="square-card__footer square-card__footer--lg">
                                    <div class="ui-picto ui-picto--faded">
                                        <i class="icon icon-plus text-primary mr-0"></i>
                                        <a href="<?php echo $blog['link']; ?>" class="link-stacked"></a>
                                    </div>
                                </footer>
                            <?php endif; ?>

                            <div class="square-card__hover-content">
                                <div class="square-card__hover-content-inner">
                                    <?php if(!empty($blog['title'])): ?>
                                        <div class="mb-2"><?=$blog['title']?></div>
                                    <?php endif; ?>
                                    <?php if(!empty($blog['text'])):
                                            echo $blog['text'];
                                        endif;
                                    if(!empty($blog['link'])):
                                    ?>
                                        <a href="<?=$blog['link']?>" class="link-stacked d-none"></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>