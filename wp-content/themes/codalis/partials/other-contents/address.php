<?php
$address = get_field('address','options');
if(!empty($address)):
?>
    <div class="text-widget mb-4 mb-sm-6 mb-lg-8">
        <address>
            <?php echo $address; ?>
        </address>
    </div>
<?php endif;