/**
 * @name: ps-counter-carousel.js
 * @desc: creating slider using swiper's library
 */

/**
 * Wrap all javascript snippets inside a javascript clousure
 * to avoid potential code conflict with
 * other libraries and codes ***
 * ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
 * Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
 * and it throws an error if it detects a statement without ending with ';' and breaks whole code!
 * To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
 */

// Wrap javascript code inside a clouser to avoid possible conflicts with other codes
;(function(w, $) {
    // == Enable strict mode using use-strict directive
    "use strict";

    // == Helper function to get the threshold value for carousel depending upon the screen sizes
    var xsMobileBreakpoint = null;
    var xmdMobileBreakpoint = null;

    if (window.isTouchDevice()) { // window.isTouchDevice, referenced to scripts.js
        xsMobileBreakpoint = 480;
        xmdMobileBreakpoint = 767;
    } else {
        xsMobileBreakpoint = 480 - 17;
        xmdMobileBreakpoint = 767 - 17;
    }

    // Function to change the number of slides according to the slideperview value in different breakpoints of the swiper object
    function _ps_getSlidesInput(_this) {
        var _threshold;
        var breakpointSlidesInput = _this.$el.attr('data-breakpoints-slides-number');
        breakpointSlidesInput = JSON.parse(breakpointSlidesInput);

        if ($(w).innerWidth() <= xsMobileBreakpoint) {
            _threshold = breakpointSlidesInput.mobile; //~3
        } else if ($(w).innerWidth() <= xmdMobileBreakpoint && $(w).innerWidth() > xsMobileBreakpoint ) {
            if (breakpointSlidesInput.hasOwnProperty('mobile_portrait')) {
                _threshold = breakpointSlidesInput.mobile_portrait;
            } else {
                _threshold = breakpointSlidesInput.mobile;
            }
        } else {
            _threshold = breakpointSlidesInput.desktop; //~6;
        }

        return _threshold;
    }

    // == Helper function to display current slides by total slides
    function _ps_countSlides(_swiper, $target) {
        var slideCount = Math.ceil((_swiper.slides.length) / _ps_getSlidesInput(_swiper));
        var currentSlideDivider = null;

        if (_swiper.slides.length >= _ps_getSlidesInput(_swiper)) {
            currentSlideDivider = 1;
        } else {
            currentSlideDivider = 0;
        }

        var currentSlide = Math.ceil(((_swiper.activeIndex - 1) % (_swiper.slides.length) + 1) / _ps_getSlidesInput(_swiper)) + currentSlideDivider; // gets current slide index

        // Update current slide count
        $target.find('.js-current-slide').html(currentSlide);
        $target.find('.js-total-slide').html(slideCount);
    }

    // Function to add class to visible slides in carousels
    function _ps_getVisibleSlidesOnCarousel(_this) {
        var indexLeft = _this.realIndex; //Index number of left slide per view in loop mode
        var indexRight = _this.realIndex + _this.params.slidesPerView; //Index number of right slide per view in loop mode

        Array.prototype.forEach.call(_this.$el[0].querySelectorAll('.counter-slider__item'), function (e) {
            e.classList.remove('is-active-carousel-slide');
        });

        $(_this.$el[0]).find('.counter-slider__item').slice(indexLeft, indexRight).addClass('is-active-carousel-slide');
    }

    // == Initialize carousel with 6 slides
    _ps_counterCarouselWithSixSlides($('.js-counter-carousel'));

    // == Build carouse with 6 slides
    function _ps_counterCarouselWithSixSlides($node) {
        if($node.length === 0) return;
        $node.each(function() {
            var $self = $(this);
            var $counterNode = $self.closest('.js-counter-carousel-parent').find('.js-counter-slider-count');

            var counterSwiper = new Swiper($self, {
                autoplay: false,
                loop: false,
                navigation: {
                    nextEl: $self.closest('.js-counter-carousel-parent').find('.js-counter-swiper-button-next'),
                    prevEl: $self.closest('.js-counter-carousel-parent').find('.js-counter-swiper-button-prev'),
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    type: 'bullets',
                },
                speed: 500,
                slidesPerView: 6,
                slidesPerGroup: 6,
                spaceBetween: 0,
                // Responsive breakpoints
                breakpoints: {
                    480: {
                        slidesPerView: 3,
                        slidesPerGroup: 3,
                        spaceBetween: 0
                    },
                    767: {
                        slidesPerView: 4,
                        slidesPerGroup: 4,
                        spaceBetween: 0
                    },
                },
                on: {
                    init: function () {
                        this.$el[0].classList.add('swiper-initialized');

                        _ps_countSlides(this, $counterNode);
                        _ps_getVisibleSlidesOnCarousel(this);
                    },
                },
            });

            counterSwiper.on('slideChange resize', function() {
                _ps_countSlides(this, $counterNode);
                _ps_getVisibleSlidesOnCarousel(this);
            });
        });
    }

    // == Initialize carousel with 2 slides
    _ps_counterCarouselWithTwoSlides($('.js-twohalf-counter-carousel'));

    // == Build carouse with 2 slides
    function _ps_counterCarouselWithTwoSlides($node) {
        if($node.length === 0) return;
        $node.each(function() {
            var $self = $(this);
            var $counterNode = $self.closest('.js-counter-carousel-parent').find('.js-counter-slider-count');

            var counterSwiper = new Swiper($self, {
                autoplay: false,
                loop: false,
                navigation: {
                    nextEl: $self.closest('.js-counter-carousel-parent').find('.js-counter-swiper-button-next'),
                    prevEl: $self.closest('.js-counter-carousel-parent').find('.js-counter-swiper-button-prev'),
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    type: 'bullets',
                },
                speed: 500,
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 90,
                // Responsive breakpoints
                breakpoints: {
                    1023: {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 40
                    },
                    767: {
                        slidesPerView: 1,
                        slidesPerGroup: 1,
                        spaceBetween: 0
                    }
                },
                on: {
                    init: function () {
                        this.$el[0].classList.add('swiper-initialized');

                        _ps_countSlides(this, $counterNode);
                        _ps_getVisibleSlidesOnCarousel(this);
                    }
                },
            });

            counterSwiper.on('slideChange resize', function() {
                _ps_countSlides(this, $counterNode);
                _ps_getVisibleSlidesOnCarousel(this);
            });
        });
    }

})(window, jQuery); // Self invoking function - IIFE