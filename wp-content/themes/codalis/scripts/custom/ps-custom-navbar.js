/**
 * @name: ps-custom-navbar.js
 */

/*!
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function (w, d, b, $) {
    /**
     * Don't write any code above this except comment because "use strict" directive won't have any effetc
     * Enable strict mode to prevent script to run in safe mode (modern way - ECMA2016)
     */
        // "use strict";

    var $doc = $('body');
    var $header = $('#mastHead');
    var isPageLoaded = false;
    var isImageLoaded = false;

    /**
     * Reset menu stuffs
     */
    window.resetNavbarOnScroll = function () {
        if (docClickFlag === false) {
            $(mobileMenuTrigger).removeClass('expanded');
            $('.' + $(mobileMenuTrigger).attr('data-target')).removeClass('shown');
        }

        if (isTouchDevice()) {
            if ($doc.hasClass('home')) {
                if (docClickFlag === false) $('.has-submenu').removeClass('is-shown is-already-shown');
            }
        } else {
            if (!$doc.hasClass('home')) {
                $('.has-submenu').removeClass('is-shown is-already-shown');
            }
        }
    };

    // == Backdrop variable
    var $menuBackdrop = $('<div />', {class: 'menu-backdrop'});
    var $backdropNode = $('.menu-backdrop');

    /**
     * Mobile menu javascript
     */
    var mobileMenuTrigger = document.querySelectorAll('.js-navbar-toggle');
    $(mobileMenuTrigger).on('click', function (e) {
        $(mobileMenuTrigger).toggleClass('expanded');
        $('.' + $(this).attr('data-target')).toggleClass('shown');
        $(b).toggleClass('menu-opened no-scroll');
        $('.is-shown').removeClass('is-shown');

        // == Set docClickFlag to true
        if ($(this).hasClass('expanded')) {
            $menuBackdrop.insertBefore($('.' + $(this).attr('data-target')));
            $backdropNode = $('.menu-backdrop');
            $backdropNode.css({'display': 'none'});
            $backdropNode.fadeIn(300);
            docClickFlag = true;
        }

        if (!$(this).hasClass('expanded')) {
            $backdropNode.fadeOut(300);
            docClickFlag = false;
        }

        e.preventDefault();
    });

    // == Close menu on click on backdrop
    $(d).on('click', '.menu-backdrop', function () {
        $(mobileMenuTrigger).removeClass('expanded');
        $('.' + $(mobileMenuTrigger).attr('data-target')).toggleClass('shown');
        $(b).removeClass('menu-opened no-scroll');
        $(this).fadeOut(300);
        $('.is-shown').removeClass('is-shown');
        docClickFlag = false;
    });

    /**
     * Build a function to toggle submenu in a navbar
     */

    var menuClicked = false;
    toggleSubmenu($('.has-submenu .js-menu-link'));

    function toggleSubmenu(_node) {
        if (_node.length === 0) return;
        _node.each(function () {
            var $self = $(this);
            $self.on('click', function (e) {
                $self.closest('.has-submenu').siblings().removeClass('is-shown');
                $self.closest('.has-submenu').toggleClass('is-shown');
                $self.closest('.has-submenu--lg').toggleClass('is-already-shown');
                $self.parent().find('.menu-item.is-active > .js-submenu-link').trigger('click');

                // == Fix overlapping issue caused by submenus

                if ($self.closest('.has-submenu').hasClass('is-shown')) {
                    $header.addClass('is-submenu-shown');
                } else {
                    setTimeout(function () {
                        $header.removeClass('is-submenu-shown');
                    }, 350);
                }

                // == Disable animation on submenu when it's already shown

                if ($('.has-submenu--lg.is-already-shown').length > 1) {
                    $self.closest('.menu').addClass('has-animation-disabled');
                    $self.closest('.has-submenu').siblings('.has-submenu--lg').removeClass('is-already-shown');
                } else {
                    $self.closest('.menu').removeClass('has-animation-disabled');
                }

                // == Remove megamenu related stuffs on click on simple dropdown menu

                if ($self.closest('.has-submenu').hasClass('has-submenu--sm')) {
                    $('.has-submenu--lg').removeClass('is-already-shown');
                }

                // == Set docClickFlag to true
                menuClicked = true;
                isPageLoaded = true;

                e.preventDefault();
            });
        });
    }

    /**
     * Function to build a submenu structure
     */

    function setSubmenuContainerHeight(self, _parentPadTop, _parentPadBtm, _parent) {
        var _nodeSubCategoryContainerHeight = parseInt(self.parent().find('.js-submenu-content-container.is-shown').outerHeight());

        // == Update final value with max value and the padding-y values of parent node
        var finalHeight = _nodeSubCategoryContainerHeight + _parentPadTop + _parentPadBtm;

        // == Set final value of the height to the parent node
        self.closest(_parent).css({
            'height': finalHeight
        });
    }

    window.ps_constructSubMenu = function (_self, _nodeSubCategoryContainer, _parent) {
        _self.each(function () {
            var $self = $(this);
            var _parentPadTop = parseInt($self.closest(_parent).css('padding-top'));
            var _parentPadBtm = parseInt($self.closest(_parent).css('padding-bottom'));

            if (isPageLoaded === false && isImageLoaded === false) {
                var image = $self.parent().find('.js-submenu-content-container.is-shown .img');
                var imageSrc = image.attr('src');


                if ( imageSrc !== undefined ) {
                    var newImage = new Image();
                    newImage.src = imageSrc;
                    image.attr('src', newImage.src);

                    newImage.addEventListener('load', function () {
                        setSubmenuContainerHeight($self, _parentPadTop, _parentPadBtm, _parent);
                        isImageLoaded = true;
                    });
                }
            } else {
                setSubmenuContainerHeight($self, _parentPadTop, _parentPadBtm, _parent);
            }
        });
    };

    ps_constructSubMenu($('.js-submenu-link'), $('.js-submenu-content-container'), $('.submenu-holder'));

    /**
     * Function to display submenus of the parent category menu
     */
    ps_constructCategoryMenu($('.js-submenu-link'));

    function ps_constructCategoryMenu(_node) {
        if (typeof _node === 'undefined') return;
        _node.each(function () {
            var $self = $(this);

            //Attach click event
            $self.on('click', function (e) {
                // Prevent default behavior of anchor tag
                e.preventDefault();

                if ($self.attr('data-target') === '') return;

                var $target = $self.closest('.js-submenu-category').find('.' + $self.attr('data-target'));

                // Expand current panel
                if (!$self.hasClass('is-active')) {
                    $self.closest('.submenu-category__item').siblings().removeClass('is-active');
                    $self.closest('.submenu-category__item').addClass('is-active');

                    $self.closest('.submenu-category__item').siblings().find('.js-submenu-content-container').removeClass('is-shown');
                    $target.addClass('is-shown');
                }

                // == Call ps_constructSubMenu on click
                ps_constructSubMenu($('.js-submenu-link'), $('.js-submenu-content-container'), $('.submenu-holder'));
            });
        });
    }

    /**
     * Hide submenus on click outside of the element (on document)
     */
    $(d).on('click', function (e) {
        if (menuClicked === false) return;

        var container = $('.has-submenu');

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.removeClass('is-already-shown is-shown');

            setTimeout(function () {
                if ($header.hasClass('is-submenu-shown')) $header.removeClass('is-submenu-shown');
            }, 350);

            menuClicked = false;
        }
    });

    /**
     * Hide mobile submenu
     */

    $('.js-menu-i-xs-current').each(function () {
        $(this).on('click', function (e) {
            $(this).closest('.is-shown').removeClass('is-shown');
            e.preventDefault();
        });
    });

    /**
     * Detect scroll direction and change the behavior of the header on page scroll
     */

    function _ps_alignRequestQuoteBtn() {
        $('.sticky-request-quote-holder').css({
            width: parseInt(($(w).innerWidth() - $('.topnavbar__c>.container').outerWidth()) / 2 + 180)
        });
    }

    _ps_alignRequestQuoteBtn();

    $(w).on('resize', function () {
        _ps_alignRequestQuoteBtn();
    });

    /**
     * Project specific javascript code to fix header overflow issue which has been set deliberately to fix submenu issues
     */

    $('.lang-dropdown .dropdown-toggle').on('click', function (e) {
        // == Fix overlapping issue caused by submenus
        if (!$(this).parent().hasClass('show')) {
            $header.addClass('is-submenu-shown');
        } else {
            setTimeout(function () {
                $header.removeClass('is-submenu-shown');
            }, 350);
        }
    });

    $('.lang-dropdown').on('hidden.bs.dropdown', function () {
        if ($('.has-submenu').hasClass('is-shown')) return;
        setTimeout(function () {
            $header.removeClass('is-submenu-shown');
        }, 350);
    });

    $('.lang-dropdown').on('shown.bs.dropdown', function () {
        if ($header.hasClass('is-submenu-shown')) {
            setTimeout(function () {
                $('.has-submenu').removeClass('is-already-shown is-shown');
            }, 350);
        }
    });

})(window, document, 'body', jQuery);