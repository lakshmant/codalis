/**
 * @name: ps-image-switcher.js
 * @desc: contains javascript code to switch image on hover actions to specific data-target element
 */

/*
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function (w, d, $, undefined) {
    /**
     * Don't write any code above this except comment because "use strict" directive won't have any effetc
     * Enable strict mode to prevent script to run in safe mode (modern way - ES6- ECMA2016)
     * Note: "use strict" directive won't work in older browsers because it's a part of ECMA2016 and it hasn't been implemented in all browsers yet. We might need to use transpiler to convert it to work in older browsers
     */

    // "use strict";

    // == Default variables
    var $jsStackedContainerNode = $('.js-stacked-card-container');
    var $jsImageHolderNode = $('.js-stacked-card-picture');

    // == Init ps_imageSwitcher
    ps_imageSwitcher($('.js-synced-lists-link'));

    /**
     * Build a function to change images on hover on specific data target elements
     */

    var $fadingNode = $('<div>', {class: 'fade-animating'});

    $('.js-stacked-card-picture').each(function() {
        $('<div>', {class: 'fade-animating'}).insertAfter($(this));
        $('.fade-animating').css({'opacity': 0, 'pointer-events': 'none'})
    });

    function ps_imageSwitcher (_node) {
        if (typeof _node === 'undefined') return;

        _node.each(function() {
           var $self = $(this);
           var dataImageSrc = $self.attr('data-img-src');

           $self.hover(function() {

               if (!$self.parent().hasClass('is-active')) {
                   $fadingNode.insertAfter($self.closest($jsStackedContainerNode).find($jsImageHolderNode));

                   setTimeout(function () {
                       $('.fade-animating').css({'opacity': 0});
                       setTimeout(function() {
                           $('.fade-animating').removeAttr('style').remove();
                       }, 350);
                   }, 50);
               }

               $self.parent().siblings().removeClass('is-active');
               $self.parent().addClass('is-active');
               $self.closest($jsStackedContainerNode).find($jsImageHolderNode).css({
                  background: 'url('+ dataImageSrc +')'
               });
           }, function() {

           });
        });
    }

})(window, document, jQuery, undefined);