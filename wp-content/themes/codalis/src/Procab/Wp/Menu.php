<?php
namespace Procab\Wp;
/**
 * Created by PhpStorm.
 * User: Laxman
 * Date: 11/14/2016
 * Time: 11:30 PM
 */

class Menu extends \Walker_Nav_Menu{

    protected $menuDesc = [];

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        if($depth == 0){
            $output .= '<ul  class="submenu-category js-submenu-category">';
        }

    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        if($depth ==0){
            $output .= "</ul>";
        }
    }

    /**
     * Starts the element output.
     *
     * @since 3.0.0
     * @since 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
     *
     * @see Walker::start_el()
     *
     * @param string   $output Used to append additional content (passed by reference).
     * @param WP_Post  $item   Menu item data object.
     * @param int      $depth  Depth of menu item. Used for padding.
     * @param stdClass $args   An object of wp_nav_menu() arguments.
     * @param int      $id     Current item ID.
     */
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $cssClass = join(" ", $item->classes);

        $cssClass = str_replace("current-menu-ancestor", "is-active", $cssClass);
        $cssClass = str_replace("current-menu-item", "is-active", $cssClass);

        $hasChild = false;
        //var_dump($item->classes);
        //current-menu-ancestor
        //current-menu-item
        if(in_array('menu-item-has-children', $item->classes)){
            $hasChild = true;
        }

        if($depth == 0) {
            if($hasChild){
                $isMenuLarge = in_array('menu-lg', $item->classes);
                if(in_array('menu-normal', $item->classes)){
                    $cssClass .= ' has-submenu--sm';
                }

                if($isMenuLarge){
                   $cssClass .= ' has-submenu--lg';
               }

                $output .= '<li class="menu__item  has-submenu '.$cssClass.' ">';
                $output .= '<a href="'.$item->url .'" class="menu__link js-menu-link">'.$item->title.' <i class="icon icon-right-chev mr-0"></i></a>';

                $output .= '<div class="submenu-holder">';
                $output .= '<div class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>'.$item->title.'</div>';

                //$output .= '<ul class="submenu-category">';
                //$output .= '<li class="submenu-category__item"><a href="" class="submenu-category__link text-uppercase">Histoire</a></li>';
                //$output .= '<li class="submenu-category__item"><a href="" class="submenu-category__link text-uppercase">Histoire</a></li></a></li>';
                //$output .= '</ul>';

                if($isMenuLarge):
                    $output .= '<div class="row submenu-grid">';
                    $output .= '<div class="col-12 col-md-4 submenu-col">';
                endif;
                //$output .= '<ul class="submenu-category js-submenu-category">';
            }else{
                $output .= "<li class='menu__item {$cssClass}'>";
                $output .= '<a href="'.$item->url .'" class="menu__link">'.$item->title.'</a>';
            }

        }

        if($depth == 1){
            //$output .= "<li {$depth} class='submenu-category__item'>";
            $output .= "<li  class='submenu-category__item {$cssClass}'>";


                if(!$hasChild):
                    $output .= '<a href="'.$item->url .'" class="submenu-category__link" >'.$item->title.'</a>';
                else:
                    $output .= '<a href="'.$item->url .'" class="submenu-category__link js-submenu-link"  data-target="submenu-'.$item->ID.'">'.$item->title.'</a>';

                    if(
                        in_array('current-menu-ancestor', $item->classes)
                        ||
                        in_array('current-menu-parent', $item->classes)
                    ){
                        $output .= ' <div class="submenu-content-container submenu-'.$item->ID.' js-submenu-content-container is-shown">';
                    }else{
                        $output .= ' <div class="submenu-content-container submenu-'.$item->ID.' js-submenu-content-container ">';
                    }


                    //start of first column
                    $output .= ' <div class="row submenu-category-grid">';
                    $output .= ' <div class="col-12 col-sm-6 submenu-category-col-sm">';

                    $output .= '<ul class="submenu-subcategory">';
                    $output .= '<li class="menu__i-xs--current js-menu-i-xs-current"><i class="icon icon-left-chev"></i>'.$item->title.'</li>';

                endif;



                /*
            $menuImg = get_field("menu_image",$item->ID);
            $image ='<div class="col-12 col-sm-6 submenu-category-col-sm">';
            $image .= '<aside class="submenu-category__desc">';
            $image .='<figure><img alt="Category Name" class="img" src="'.$menuImg.'" /></figure>';
            $image .='<blockquote><p>'.$item->description.'</p></blockquote>';
            $image .='</aside></div>';
                */
           // $output .= $image;
        }

        if($depth == 2){
            $output .= "<li  class='submenu-category__item {$cssClass}'>";
            //$output .= '<li class="submenu-category__item">';
            $output .= '<a href="'.$item->url .'" class="submenu-category__link ">'.$item->title.'</a>';
            //$output .=$image;
        }

    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $hasChild = false;
        if(in_array('menu-item-has-children', $item->classes)){
            $hasChild = true;
        }
        $isMenuLarge = in_array('menu-lg', $item->classes);
        if($depth == 0) {
            if($isMenuLarge) $output .='</div></div>';
            if($hasChild) $output .= '</div>';

            $output .= '</li>';
        }
        if($depth == 1) {
            if($hasChild) {


            $menuImg = get_field("menu_image",$item->ID);
            $image ='<div class="col-12 col-sm-6 submenu-category-col-sm">';
            $image .= '<aside class="submenu-category__desc">';
            $image .='<figure><img alt="Category Name" class="img" src="'.$menuImg.'" /></figure>';
            $image .='<blockquote><p>'.$item->description.'</p></blockquote>';
            $image .='</aside></div>';



                $output .= '</ul>';
                $output .= '</div>';

                $output .= $image;

                $output .='</div></div></li>';
            }else{
                $output .= '</li>';
           }
           // $output .= '</li>';
        }
        if($depth == 2) {
            $output .= '</li>';
        }
    }


    /**
     * Starts the element output.
     *
     * @since 3.0.0
     * @since 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
     *
     * @see Walker::start_el()
     *
     * @param string   $output Used to append additional content (passed by reference).
     * @param WP_Post  $item   Menu item data object.
     * @param int      $depth  Depth of menu item. Used for padding.
     * @param stdClass $args   An object of wp_nav_menu() arguments.
     * @param int      $id     Current item ID.
     */
    public function start_el_x( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $cssClasses = [];
        if($depth == 0) $cssClasses[] = 'list-inline-item';

        if(in_array('menu-item-has-children', $item->classes)){
            $cssClasses[] = 'menu-item-has-children';
        }
        //current-menu-item

        //var_dump($item);

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

        $cssClasses = join(" ", $cssClasses);
        $output .= "<li class='{$cssClasses}'>";


        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $output .= "<a {$attributes}>{$item->title}</a>";

    }

}