<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Procab\\' => array($baseDir . '/Procab'),
    'App\\' => array($baseDir . '/App'),
);
